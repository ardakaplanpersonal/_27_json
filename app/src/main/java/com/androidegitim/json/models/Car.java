package com.androidegitim.json.models;

/**
 * Created by Arda Kaplan on 10.02.2018 - 12:41
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Car {

    private String mark;
    private String plateNo;

    public Car(String mark, String plateNo) {
        this.mark = mark;
        this.plateNo = plateNo;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", plateNo='" + plateNo + '\'' +
                '}';
    }
}
