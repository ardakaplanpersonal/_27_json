package com.androidegitim.json;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.httprequestlibrary.helpers.JsonHelpers;
import com.androidegitim.json.models.Car;
import com.androidegitim.json.models.Person;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);

        takeObjectFromJson();

//        createManuelJSON();

//        createPersons();
    }


    private void takeObjectFromJson() {

        String source = "[{\"name\":\"arda\",\"age\":25,\"hasDrivingLicense\":true,\"cars\":[{\"mark\":\"Araba 0\",\"plateNo\":\"Plaka 0\"},{\"mark\":\"Araba 1\",\"plateNo\":\"Plaka 1\"}]},{\"name\":\"Ahmet\",\"age\":15,\"hasDrivingLicense\":false}]";

        RDALogger.info("Kaynak JSON " + source);

//        takeObjectFromManuel(source);

        takeObjectFromGson(source);

    }

    private void takeObjectFromManuel(String source) {


        try {

            JSONArray personJSonArray = new JSONArray(source);

            ArrayList<Person> personArrayList = new ArrayList<>();

            for (int i = 0; i < personJSonArray.length(); i++) {

                JSONObject personJsonObject = personJSonArray.getJSONObject(i);

                Person person = new Person();

                person.setAge(personJsonObject.getInt("age"));
                person.setName(personJsonObject.getString("name"));
                person.setHasDrivingLicense(personJsonObject.getBoolean("hasDrivingLicense"));

                if (personJsonObject.has("cars")) {

                    JSONArray carsJsonArray = personJsonObject.getJSONArray("cars");

                    ArrayList<Car> carsArrayList = new ArrayList<>();

                    for (int j = 0; j < carsJsonArray.length(); j++) {

                        JSONObject carJsonObject = carsJsonArray.getJSONObject(j);

                        Car car = new Car(carJsonObject.getString("mark"), carJsonObject.getString("plateNo"));

                        carsArrayList.add(car);
                    }

                    person.setCars(carsArrayList);
                }

                personArrayList.add(person);
            }


            RDALogger.info("DÖNÜŞTÜRÜLEN " + personArrayList);


        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    private void takeObjectFromGson(String source) {

        Type collectionType = new TypeToken<List<Person>>() {
        }.getType();

        RDALogger.info(JsonHelpers.jsonToList(source, collectionType));


        //tek json objesi çevirmek için
        String ahmetJsonObject = "{\"name\":\"Ahmet\",\"age\":15,\"hasDrivingLicense\":false}";
        RDALogger.info(JsonHelpers.jsonToObject(ahmetJsonObject, Person.class));
    }


    private void createManuelJSON() {

        ArrayList<Person> personArrayList = new ArrayList<>();

        Person ardaPerson = new Person();

        ardaPerson.setName("arda");
        ardaPerson.setAge(25);
        ardaPerson.setHasDrivingLicense(true);

        ArrayList<Car> cars = new ArrayList<>();

        for (int i = 0; i < 2; i++) {

            Car car = new Car("Araba " + i, "Plaka " + i);

            cars.add(car);
        }

        ardaPerson.setCars(cars);

        personArrayList.add(ardaPerson);


        /////////////////////////////////////////////////////
        Person ahmetPerson = new Person();

        ahmetPerson.setName("Ahmet");
        ahmetPerson.setAge(15);
        ahmetPerson.setHasDrivingLicense(false);

        personArrayList.add(ahmetPerson);


        try {

            JSONArray personJsonArray = new JSONArray();

            for (int i = 0; i < personArrayList.size(); i++) {

                JSONObject personJsonObject = new JSONObject();

                personJsonObject.put("name", personArrayList.get(i).getName());

                personJsonObject.put("age", personArrayList.get(i).getAge());

                personJsonObject.put("hasDrivingLicense", personArrayList.get(i).isHasDrivingLicense());


                if (personArrayList.get(i).getCars() != null) {

                    JSONArray carsJSONArray = new JSONArray();

                    for (int j = 0; j < personArrayList.get(i).getCars().size(); j++) {

                        Car car = personArrayList.get(i).getCars().get(j);

                        JSONObject carJsonObject = new JSONObject();

                        carJsonObject.put("mark", car.getMark());

                        carJsonObject.put("plateNo", car.getPlateNo());

                        carsJSONArray.put(carJsonObject);
                    }

                    personJsonObject.put("cars", carsJSONArray);
                }


                personJsonArray.put(personJsonObject);
            }

            RDALogger.info(personJsonArray);


        } catch (JSONException e) {

            e.printStackTrace();
        }
    }


    private void createPersons() {

        ArrayList<Person> personArrayList = new ArrayList<>();

        Person ardaPerson = new Person();

        ardaPerson.setName("arda");
        ardaPerson.setAge(25);
        ardaPerson.setHasDrivingLicense(true);

        ArrayList<Car> cars = new ArrayList<>();

        for (int i = 0; i < 2; i++) {

            Car car = new Car("Araba " + i, "Plaka " + i);

            cars.add(car);
        }

        ardaPerson.setCars(cars);

        personArrayList.add(ardaPerson);


        /////////////////////////////////////////////////////
        Person ahmetPerson = new Person();

        ahmetPerson.setName("Ahmet");
        ahmetPerson.setAge(15);
        ahmetPerson.setHasDrivingLicense(false);

        personArrayList.add(ahmetPerson);


        RDALogger.info(JsonHelpers.listToJson(personArrayList));

//        RDALogger.info(ardaPerson);
//
//        RDALogger.info(JsonHelpers.objectToJson(ardaPerson));

    }
}
